## Сервис для хранения основных справочников и таблиц

### Для локального запуска

```sh
# 1. Должен быть установлен Docker, Docker-compose
# 2. Запустить docker-compose c БД "postgres_dev"
$ docker-compose -f src/main/resources/docker-compose.yml up -d
```
```sh
# 3. Запустить приложение, установив Active profiles: dev или выполнив команду
$ ./gradlew bootRun --args='--spring.profiles.active=dev'
```


### Для запусков тестов:

```sh
# 1. Запустить docker-compose c БД "postgres_test"
$ docker-compose -f src/test/resources/docker-compose.yml up -d
```
```sh
# 2. Выполнить команду `./gradlew test`
$ ./gradlew test
```

### Для запуска приложения в Docker container;
1. Запуск работы приложения. Выполнить команду `./gradlew composeUp`
2. Остановка работы приложения. Выполнить команду `./gradlew composeDown`

### SWAGGER
http://localhost:8075/swagger-ui/index.html