package com.example.datatodb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataToDbApplication {

	public static void main(String[] args) {
		SpringApplication.run(DataToDbApplication.class, args);
	}

}
