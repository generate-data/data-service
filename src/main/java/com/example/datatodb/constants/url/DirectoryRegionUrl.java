package com.example.datatodb.constants.url;

/**
 * URL for DirectoryRegion Entity
 */
public final class DirectoryRegionUrl {
    public final static String GET_ALL = "/directory/regions";
    public final static String GET_BY_REGION = "/directory/regions/search/find-by-region";
}
