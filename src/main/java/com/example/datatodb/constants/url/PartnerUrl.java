package com.example.datatodb.constants.url;

/**
 * URL for Partner Entity
 */
public final class PartnerUrl {
    public final static String GET_ALL = "/partners";
    public final static String GET_BY_KEY = "/partners/search/find-by-key";
    public final static String GET_ADVANCED_SEARCH = "/partners/search/advanced-search";
}
