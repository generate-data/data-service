package com.example.datatodb.constants.url;

public final class ApiUrl {
    public final static String GET_API = "api";
    public final static String GET_VERSION = "/v1";
}
