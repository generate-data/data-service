package com.example.datatodb.constants.url;

/**
 * URL for DirectoryCity Entity
 */
public final class DirectoryCityUrl {
    public final static String GET_ALL = "/directory/cities";
    public final static String GET_BY_CITY = "/directory/cities/search/find-by-city";
}
