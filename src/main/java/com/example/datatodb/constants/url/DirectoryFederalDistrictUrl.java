package com.example.datatodb.constants.url;

/**
 * URL for DirectoryCity Entity
 */
public class DirectoryFederalDistrictUrl {
    public final static String GET_ALL = "/directory/districts";
    public final static String GET_BY_DISTRICT = "/directory/districts/search/find-by-district";
}
