package com.example.datatodb.constants.exceptionMessage;

public final class DirectoryRegionExceptionMessage {
    public static final String REGION_NOT_FOUND_BY_ID = "Регион %s не найден по id";
    public static final String REGION_NOT_FOUND_BY_NAME = "Регион %s не найден по названию";
    public static final String REGION_ALREADY_EXISTS = "Регион %s уже есть в бд";
}
