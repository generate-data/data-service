package com.example.datatodb.constants.exceptionMessage;

public final class PartnerExceptionMessage {
    public static final String PARTNER_NOT_FOUND_BY_KEY = "Контрагент %s не найден по key";
}
