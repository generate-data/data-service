package com.example.datatodb.constants.exceptionMessage;

public final class DirectoryFederalDistrictExceptionMessage {
    public static final String DISTRICT_NOT_FOUND_BY_ID = "Федеральный округ %s не найден по id";
    public static final String DISTRICT_NOT_FOUND_BY_NAME = "Федеральный округ %s не найден по названию";
    public static final String DISTRICT_ALREADY_EXISTS = "Федеральный округ %s уже есть в бд";

}
