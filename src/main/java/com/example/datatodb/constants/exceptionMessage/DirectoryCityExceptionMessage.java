package com.example.datatodb.constants.exceptionMessage;

public final class DirectoryCityExceptionMessage {
    public static final String CITY_NOT_FOUND_BY_ID = "Город %s не найден по id";
    public static final String CITY_NOT_FOUND_BY_NAME = "Город %s не найден по названию";
    public static final String CITY_ALREADY_EXISTS = "Город %s уже есть в бд";
}
