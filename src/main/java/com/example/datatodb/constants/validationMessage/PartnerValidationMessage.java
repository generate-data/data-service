package com.example.datatodb.constants.validationMessage;

public final class PartnerValidationMessage {
    public static final String PARTNER_NAME_NOT_BLANK = "Название контрагента должно быть заполненно";
    public static final String PARTNER_INN_NOT_BLANK = "ИНН должно быть заполненно";
    public static final String PARTNER_INN_LENGTH = "Максимальная длина 10 цифр";
}
