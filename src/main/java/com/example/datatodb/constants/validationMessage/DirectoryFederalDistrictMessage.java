package com.example.datatodb.constants.validationMessage;

public final class DirectoryFederalDistrictMessage {
    public static final String DISTRICT_NOT_BLANK = "Название федерального округа должно быть заполнено";
    public static final String DISTRICT_LENGTH = "Максимальная длинна 50 символов";
    public static final String DISTRICT_ID_NOT_NULL = "ID должно быть заполнено";
}
