package com.example.datatodb.constants.validationMessage;

public final class DirectoryCityValidationMessage {
    public static final String CITY_NOT_BLANK = "Название города должно быть заполнено";
    public static final String CITY_LENGTH = "Максимальная длинна 50 символов";
    public static final String ID_NOT_NULL = "ID должно быть заполнено";
}
