package com.example.datatodb.constants.validationMessage;

public final class DirectoryRegionValidationMessage {
    public static final String REGION_NOT_BLANK = "Название региона должно быть заполнено";
    public static final String REGION_LENGTH = "Максимальная длинна 50 символов";
    public static final String ID_NOT_NULL = "ID должно быть заполнено";
}
