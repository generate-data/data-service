package com.example.datatodb.service;

import com.example.datatodb.constants.exceptionMessage.DirectoryRegionExceptionMessage;
import com.example.datatodb.dto.DirectoryRegion.AddDirectoryRegionRequestDTO;
import com.example.datatodb.dto.DirectoryRegion.GetDirectoryRegionResponseDTO;
import com.example.datatodb.dto.DirectoryRegion.GetDirectoryRegionsResponseDTO;
import com.example.datatodb.dto.DirectoryRegion.UpdateDirectoryRegionRequestDTO;
import com.example.datatodb.exception.EntityExistsException;
import com.example.datatodb.exception.NoSuchElementException;
import com.example.datatodb.mapper.DirectoryRegion.DirectoryRegionMapper;
import com.example.datatodb.mapper.DirectoryRegion.DirectoryRegionsMapper;
import com.example.datatodb.model.DirectoryRegion;
import com.example.datatodb.repository.DirectoryRegionRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DirectoryRegionService {
    private final DirectoryRegionRepository directoryRegionRepository;

    private final DirectoryRegionMapper directoryRegionMapper;
    private final DirectoryRegionsMapper directoryRegionsMapper;

    /**
     * Get all regions from table 'generate_directory_regions'
     *
     * @return {@link GetDirectoryRegionsResponseDTO}
     */
    public GetDirectoryRegionsResponseDTO findAll() {
        List<DirectoryRegion> all = directoryRegionRepository.findAll(Sort.by("region"));
        return directoryRegionsMapper.mapToDTO(all);
    }

    /**
     * Get DirectoryRegion by region name
     *
     * @param region field 'region' table 'generate_directory_regions'
     * @return {@link Optional}<{@link GetDirectoryRegionResponseDTO}>
     */
    public GetDirectoryRegionResponseDTO findByRegionName(String region) {
        return directoryRegionRepository.findByRegion(region)
                .map(directoryRegionMapper::mapToDTO)
                .orElseThrow(() -> new NoSuchElementException(DirectoryRegionExceptionMessage.REGION_NOT_FOUND_BY_NAME.formatted(region)));
    }

    /**
     * Add new DirectoryRegion Entity
     *
     * @param request {@link AddDirectoryRegionRequestDTO}
     * @return {@link DirectoryRegion}
     */
    @Transactional
    public DirectoryRegion addNew(AddDirectoryRegionRequestDTO request) {
        Optional<DirectoryRegion> optional = directoryRegionRepository.findByRegion(request.region());
        if (optional.isPresent()) {
            throw new EntityExistsException(DirectoryRegionExceptionMessage.REGION_ALREADY_EXISTS.formatted(request.region()));
        }

        return Optional.of(request)
                .map(directoryRegionMapper::ToEntity)
                .map(this::enrichDirectoryRegion)
                .map(directoryRegionRepository::save)
                .orElseThrow();

    }

    /**
     * Update DirectoryRegion by id
     *
     * @param request {@link UpdateDirectoryRegionRequestDTO}
     * @return {@link GetDirectoryRegionResponseDTO}
     */
    @Transactional
    public GetDirectoryRegionResponseDTO update(UpdateDirectoryRegionRequestDTO request) {
        Optional<DirectoryRegion> optional = directoryRegionRepository.findById(request.id());
        if (optional.isEmpty()) {
            throw new NoSuchElementException(DirectoryRegionExceptionMessage.REGION_NOT_FOUND_BY_ID.formatted(request.id()));
        }

        return optional
                .map(entity -> directoryRegionMapper.copyToEntity(request, entity))
                .map(directoryRegionRepository::save)
                .map(directoryRegionMapper::mapToDTO)
                .orElseThrow();

    }

    private DirectoryRegion enrichDirectoryRegion(DirectoryRegion directoryRegion) {
        directoryRegion.setCreationDate(ZonedDateTime.now());
        directoryRegion.setRegion(directoryRegion.getRegion().trim());
        return directoryRegion;
    }

}
