package com.example.datatodb.service;

import com.example.datatodb.constants.exceptionMessage.DirectoryFederalDistrictExceptionMessage;
import com.example.datatodb.dto.DirectoryFederalDistrict.AddDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.GetDirectoryFederalDistrictResponseDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.GetDirectoryFederalDistrictsResponseDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.UpdateDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.exception.EntityExistsException;
import com.example.datatodb.exception.NoSuchElementException;
import com.example.datatodb.mapper.DirectoryFederalDistrict.DirectoryFederalDistrictMapper;
import com.example.datatodb.mapper.DirectoryFederalDistrict.DirectoryFederalDistrictsMapper;
import com.example.datatodb.mapper.DirectoryFederalDistrict.UpdateDirectoryFederalDistrictMapper;
import com.example.datatodb.model.DirectoryFederalDistrict;
import com.example.datatodb.repository.DirectoryFederalDistrictRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DirectoryFederalDistrictService {
    private final DirectoryFederalDistrictRepository directoryFederalDistrictRepository;

    private final DirectoryFederalDistrictsMapper directoryFederalDistrictsMapper;
    private final DirectoryFederalDistrictMapper directoryFederalDistrictMapper;
    private final UpdateDirectoryFederalDistrictMapper updateDirectoryFederalDistrictMapper;


    /**
     * Get all federal districts from table 'generate_directory_federal_districts'
     *
     * @return {@link GetDirectoryFederalDistrictsResponseDTO}
     */
    public GetDirectoryFederalDistrictsResponseDTO findAll() {
        List<DirectoryFederalDistrict> all = directoryFederalDistrictRepository.findAll(Sort.by("district"));
        return directoryFederalDistrictsMapper.mapToDTO(all);
    }

    /**
     * Get DirectoryFederalDistrict by district name
     *
     * @param districtName field 'district' table 'generate_directory_federal_districts'
     * @return {@link GetDirectoryFederalDistrictResponseDTO}
     */
    public GetDirectoryFederalDistrictResponseDTO findByDistrictName(String districtName) {
        return directoryFederalDistrictRepository.findByDistrict(districtName)
                .map(directoryFederalDistrictMapper::toDTO)
                .orElseThrow(() -> new NoSuchElementException(DirectoryFederalDistrictExceptionMessage.DISTRICT_NOT_FOUND_BY_NAME.formatted(districtName)));
    }

    /**
     * Add new DirectoryFederalDistrict Entity
     *
     * @param request {@link AddDirectoryFederalDistrictRequestDTO}
     * @return {@link DirectoryFederalDistrict}
     */
    @Transactional
    public DirectoryFederalDistrict addNew(AddDirectoryFederalDistrictRequestDTO request) {
        Optional<DirectoryFederalDistrict> optional = directoryFederalDistrictRepository.findByDistrict(request.district());
        if (optional.isPresent()) {
            throw new EntityExistsException(DirectoryFederalDistrictExceptionMessage.DISTRICT_ALREADY_EXISTS.formatted(request.district()));
        }

        return Optional.of(request)
                .map(directoryFederalDistrictMapper::toEntity)
                .map(this::enrichDirectoryFederalDistrict)
                .map(directoryFederalDistrictRepository::save)
                .orElseThrow();
    }

    /**
     * Update DirectoryFederalDistrict by id
     *
     * @param requestDTO {@link UpdateDirectoryFederalDistrictRequestDTO}
     * @return {@link GetDirectoryFederalDistrictResponseDTO}
     */
    @Transactional
    public GetDirectoryFederalDistrictResponseDTO update(UpdateDirectoryFederalDistrictRequestDTO requestDTO) {
        Optional<DirectoryFederalDistrict> optional = directoryFederalDistrictRepository.findById(requestDTO.id());
        if (optional.isEmpty()) {
            throw new NoSuchElementException(DirectoryFederalDistrictExceptionMessage.DISTRICT_NOT_FOUND_BY_ID.formatted(requestDTO.id()));
        }

        return optional
                .map(entity -> updateDirectoryFederalDistrictMapper.copyToEntity(requestDTO, entity))
                .map(directoryFederalDistrictRepository::save)
                .map(directoryFederalDistrictMapper::toDTO)
                .orElseThrow();

    }

    private DirectoryFederalDistrict enrichDirectoryFederalDistrict(DirectoryFederalDistrict district) {
        district.setCreationDate(ZonedDateTime.now());
        district.setDistrict(district.getDistrict().trim());
        return district;
    }

}
