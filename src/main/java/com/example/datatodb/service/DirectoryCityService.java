package com.example.datatodb.service;

import com.example.datatodb.constants.exceptionMessage.DirectoryCityExceptionMessage;
import com.example.datatodb.dto.DirectoryCity.AddDirectoryCityRequestDTO;
import com.example.datatodb.dto.DirectoryCity.GetDirectoryCitiesResponseDTO;
import com.example.datatodb.dto.DirectoryCity.GetDirectoryCityResponseDTO;
import com.example.datatodb.dto.DirectoryCity.UpdateDirectoryCityRequestDTO;
import com.example.datatodb.exception.EntityExistsException;
import com.example.datatodb.exception.NoSuchElementException;
import com.example.datatodb.mapper.DirectoryCity.DirectoryCitiesMapper;
import com.example.datatodb.mapper.DirectoryCity.DirectoryCityMapper;
import com.example.datatodb.mapper.DirectoryCity.UpdateDirectoryCityMapper;
import com.example.datatodb.model.DirectoryCity;
import com.example.datatodb.repository.DirectoryCityRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class DirectoryCityService {
    private final DirectoryCityRepository directoryCityRepository;

    private final DirectoryCitiesMapper directoryCitiesMapperOld;
    private final DirectoryCityMapper directoryCityMapper;
    private final UpdateDirectoryCityMapper updateDirectoryCityMapper;

    /**
     * Get all cities from table 'generate_directory_cities'
     *
     * @return {@link GetDirectoryCitiesResponseDTO}
     */
    public GetDirectoryCitiesResponseDTO findAll() {
        List<DirectoryCity> all = directoryCityRepository.findAll(Sort.by("city"));
        return directoryCitiesMapperOld.mapToDTO(all);
    }

    /**
     * Get DirectoryCity by city name
     *
     * @param cityName field 'city' table 'generate_directory_cities'
     * @return {@link Optional}<{@link GetDirectoryCityResponseDTO}>
     */
    public GetDirectoryCityResponseDTO findByCityName(String cityName) {
        return directoryCityRepository.findByCity(cityName)
                .map(directoryCityMapper::toDTO)
                .orElseThrow(() -> new NoSuchElementException(DirectoryCityExceptionMessage.CITY_NOT_FOUND_BY_NAME.formatted(cityName)));
    }

    /**
     * Add new DirectoryCity Entity
     *
     * @param request {@link AddDirectoryCityRequestDTO}
     * @return {@link DirectoryCity}
     */
    @Transactional
    public DirectoryCity addNew(AddDirectoryCityRequestDTO request) {
        Optional<DirectoryCity> optional = directoryCityRepository.findByCity(request.city());
        if (optional.isPresent()) {
            log.error(DirectoryCityExceptionMessage.CITY_ALREADY_EXISTS.formatted(request.city()));
            throw new EntityExistsException(DirectoryCityExceptionMessage.CITY_ALREADY_EXISTS.formatted(request.city()));
        }

        return Optional.of(request)
                .map(directoryCityMapper::toEntity)
                .map(this::enrichDirectoryCity)
                .map(directoryCityRepository::save)
                .orElseThrow();
    }

    /**
     * Update DirectoryCity by id
     *
     * @param requestDTO {@link UpdateDirectoryCityRequestDTO}
     * @return {@link GetDirectoryCityResponseDTO}
     */
    @Transactional
    public GetDirectoryCityResponseDTO update(UpdateDirectoryCityRequestDTO requestDTO) {
        Optional<DirectoryCity> optional = directoryCityRepository.findById(requestDTO.id());
        if (optional.isEmpty()) {
            throw new NoSuchElementException(DirectoryCityExceptionMessage.CITY_NOT_FOUND_BY_ID.formatted(requestDTO));
        }

        return optional
                .map(entity -> updateDirectoryCityMapper.copyToEntity(requestDTO, entity))
                .map(directoryCityRepository::save)
                .map(directoryCityMapper::toDTO)
                .orElseThrow();

    }

    private DirectoryCity enrichDirectoryCity(DirectoryCity directoryCity) {
        directoryCity.setCreationDate(ZonedDateTime.now());
        directoryCity.setCity(directoryCity.getCity().trim());
        return directoryCity;
    }
}
