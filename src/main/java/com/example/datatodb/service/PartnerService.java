package com.example.datatodb.service;

import com.example.datatodb.constants.exceptionMessage.PartnerExceptionMessage;
import com.example.datatodb.constants.key.Key;
import com.example.datatodb.dto.Partner.*;
import com.example.datatodb.exception.NoSuchElementException;
import com.example.datatodb.mapper.Partner.PartnerMapper;
import com.example.datatodb.mapper.Partner.PartnersMapper;
import com.example.datatodb.model.Partner;
import com.example.datatodb.repository.PartnersRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
@Transactional(readOnly = true)
public class PartnerService {
    private final PartnersRepository partnersRepository;

    private final PartnerMapper partnerMapper;
    private final PartnersMapper partnersMapper;

    /**
     * Get all partners from table 'test_partners'
     *
     * @return {@link GetPartnersResponseDTO}
     */
    public GetPartnersResponseDTO findAll() {
        List<Partner> all = partnersRepository.findAll(Sort.by("partnerName"));
        return partnersMapper.mapToDTO(all);
    }

    /**
     * Get partner by key
     *
     * @param key field 'partner_key' table 'test_partners'
     * @return {@link Optional<GetPartnersResponseDTO>}
     */
    public GetPartnerResponseDTO findByKey(String key) {
        return partnersRepository.findByKey(key)
                .map(partnerMapper::ToDTO)
                .orElseThrow(() -> new NoSuchElementException(PartnerExceptionMessage.PARTNER_NOT_FOUND_BY_KEY.formatted(key)));
    }

    /**
     * Get all partners by advanced search
     *
     * @param request {@link GetPartnerAdvancedSearchRequestDTO}
     * @return {@link GetPartnersResponseDTO}
     */
    public GetPartnersResponseDTO findByAdvancedSearch(GetPartnerAdvancedSearchRequestDTO request) {
        List<Partner> partners = partnersRepository.advancedSearch(request.key(), request.partnerName(), request.partnerInn(), request.active());
        return partnersMapper.mapToDTO(partners);
    }

    /**
     * Add new Partner Entity
     *
     * @param request {@link AddPartnerRequestDTO}
     * @return {@link Partner}
     */
    @Transactional
    public GetPartnerResponseDTO addNew(AddPartnerRequestDTO request) {

        return Optional.of(request)
                .map(partnerMapper::ToEntity)
                .map(this::enrichPartner)
                .map(partnersRepository::save)
                .map(partnerMapper::ToDTO)
                .orElseThrow();

    }

    /**
     * Update partner by key
     *
     * @param request {@link UpdatePartnerRequestDTO }
     * @return {@link Partner}
     */
    @Transactional
    public GetPartnerResponseDTO updatePartner(UpdatePartnerRequestDTO request) {
        Optional<Partner> optional = partnersRepository.findByKey(request.key());
        if (optional.isEmpty()){
            throw new NoSuchElementException(PartnerExceptionMessage.PARTNER_NOT_FOUND_BY_KEY.formatted(request.key()));
        }

        return optional
                .map(entity->partnerMapper.copyToEntity(request,entity))
                .map(partnersRepository::save)
                .map(partnerMapper::ToDTO)
                .orElseThrow();

    }

    private Partner enrichPartner(Partner partner) {
        partner.setKey(Key.PARTNER_KEY + UUID.randomUUID());
        partner.setCreationDate(ZonedDateTime.now());
        partner.setActive(1);
        return partner;
    }
}
