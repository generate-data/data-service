package com.example.datatodb.dto.Partner;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record GetPartnersResponseDTO(
        @JsonProperty("partners")
        List<GetPartnerResponseDTO> list
) {
}
