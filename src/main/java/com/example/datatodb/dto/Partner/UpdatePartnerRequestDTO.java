package com.example.datatodb.dto.Partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

@JsonIgnoreProperties(ignoreUnknown = true)
public record UpdatePartnerRequestDTO(
        @JsonProperty("key")
        String key,
        @JsonProperty("partner_name")
        String partnerName,
        @JsonProperty("partner_inn")
        String partnerInn,
        @JsonProperty("active")
        Integer active
) {
}
