package com.example.datatodb.dto.Partner;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AddPartnerResponseDTO(
        @JsonProperty("key")
        String key
) {
}
