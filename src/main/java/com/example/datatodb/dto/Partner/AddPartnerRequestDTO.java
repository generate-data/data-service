package com.example.datatodb.dto.Partner;

import com.example.datatodb.constants.validationMessage.PartnerValidationMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record AddPartnerRequestDTO(
        @JsonProperty("partner_name")
        @NotBlank(message = PartnerValidationMessage.PARTNER_NAME_NOT_BLANK)
        String partnerName,

        @JsonProperty("partner_inn")
        @NotBlank(message = PartnerValidationMessage.PARTNER_INN_NOT_BLANK)
        @Length(max = 10, message = PartnerValidationMessage.PARTNER_INN_LENGTH)
        String partnerInn
) {

}
