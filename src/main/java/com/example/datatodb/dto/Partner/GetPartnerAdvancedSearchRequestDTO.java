package com.example.datatodb.dto.Partner;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
@JsonIgnoreProperties(ignoreUnknown = true)
public record GetPartnerAdvancedSearchRequestDTO(
        @JsonProperty("key")
        String key,
        @JsonProperty("partner_name")
        String partnerName,
        @JsonProperty("partner_inn")
        String partnerInn,
        @JsonProperty("active")
        Integer active
) {
}
