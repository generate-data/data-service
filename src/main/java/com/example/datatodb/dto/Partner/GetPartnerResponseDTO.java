package com.example.datatodb.dto.Partner;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;

@Builder
public record GetPartnerResponseDTO(
        @JsonProperty("key")
        String key,
        @JsonProperty("creation_date")
        String creationDate,
        @JsonProperty("partner_name")
        String partnerName,
        @JsonProperty("partner_inn")
        String partnerInn,
        @JsonProperty("active")
        Integer active
) {
}
