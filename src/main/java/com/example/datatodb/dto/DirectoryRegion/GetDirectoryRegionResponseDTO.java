package com.example.datatodb.dto.DirectoryRegion;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetDirectoryRegionResponseDTO(
        @JsonProperty("id")
        Integer id,
        @JsonProperty("region")
        String region
) {
}
