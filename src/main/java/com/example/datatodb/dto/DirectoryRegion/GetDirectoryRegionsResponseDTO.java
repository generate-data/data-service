package com.example.datatodb.dto.DirectoryRegion;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record GetDirectoryRegionsResponseDTO(
        @JsonProperty("regions")
        List<GetDirectoryRegionResponseDTO> list
) {
}
