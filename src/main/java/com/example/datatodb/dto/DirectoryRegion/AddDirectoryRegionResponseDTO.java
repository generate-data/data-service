package com.example.datatodb.dto.DirectoryRegion;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AddDirectoryRegionResponseDTO(
        @JsonProperty("id")
        Integer id
) {
}
