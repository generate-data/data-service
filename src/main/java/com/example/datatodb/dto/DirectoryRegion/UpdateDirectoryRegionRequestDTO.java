package com.example.datatodb.dto.DirectoryRegion;

import com.example.datatodb.constants.validationMessage.DirectoryRegionValidationMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public record UpdateDirectoryRegionRequestDTO(
        @JsonProperty("id")
        @NotNull(message = DirectoryRegionValidationMessage.ID_NOT_NULL)
        Integer id,

        @JsonProperty("region")
        @NotBlank(message = DirectoryRegionValidationMessage.REGION_NOT_BLANK)
        @Length(max = 50, message = DirectoryRegionValidationMessage.REGION_LENGTH)
        String region
) {
}
