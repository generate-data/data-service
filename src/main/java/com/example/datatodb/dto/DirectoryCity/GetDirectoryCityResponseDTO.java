package com.example.datatodb.dto.DirectoryCity;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetDirectoryCityResponseDTO(
        @JsonProperty("id")
        Integer id,
        @JsonProperty("city")
        String city
) {
}
