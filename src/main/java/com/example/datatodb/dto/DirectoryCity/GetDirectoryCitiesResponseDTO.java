package com.example.datatodb.dto.DirectoryCity;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record GetDirectoryCitiesResponseDTO(
        @JsonProperty("cities")
        List<GetDirectoryCityResponseDTO> list
) {
}
