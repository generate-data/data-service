package com.example.datatodb.dto.DirectoryCity;

import com.example.datatodb.constants.validationMessage.DirectoryCityValidationMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@JsonIgnoreProperties(ignoreUnknown = true)
public record UpdateDirectoryCityRequestDTO(
        @JsonProperty("id")
        @NotNull(message = DirectoryCityValidationMessage.ID_NOT_NULL)
        Integer id,

        @JsonProperty("city")
        @NotBlank(message = DirectoryCityValidationMessage.CITY_NOT_BLANK)
        @Length(max = 50, message = DirectoryCityValidationMessage.CITY_LENGTH)
        String city
) {
}
