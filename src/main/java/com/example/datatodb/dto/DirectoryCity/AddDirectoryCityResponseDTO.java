package com.example.datatodb.dto.DirectoryCity;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AddDirectoryCityResponseDTO(
        @JsonProperty("id")
        Integer id
) {
}
