package com.example.datatodb.dto.DirectoryFederalDistrict;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public record GetDirectoryFederalDistrictsResponseDTO(
        @JsonProperty("federal-districts")
        List<GetDirectoryFederalDistrictResponseDTO> list
) {
}
