package com.example.datatodb.dto.DirectoryFederalDistrict;

import com.fasterxml.jackson.annotation.JsonProperty;

public record AddDirectoryFederalDistrictResponseDTO(
        @JsonProperty("id")
        Integer id
) {
}
