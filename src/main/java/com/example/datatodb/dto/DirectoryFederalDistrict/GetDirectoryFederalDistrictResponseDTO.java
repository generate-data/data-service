package com.example.datatodb.dto.DirectoryFederalDistrict;

import com.fasterxml.jackson.annotation.JsonProperty;

public record GetDirectoryFederalDistrictResponseDTO(
        @JsonProperty("id")
        Integer id,
        @JsonProperty("district")
        String district
) {
}
