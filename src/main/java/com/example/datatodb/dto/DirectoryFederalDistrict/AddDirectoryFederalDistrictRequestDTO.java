package com.example.datatodb.dto.DirectoryFederalDistrict;

import com.example.datatodb.constants.validationMessage.DirectoryFederalDistrictMessage;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;

@JsonIgnoreProperties(ignoreUnknown = true)
public record AddDirectoryFederalDistrictRequestDTO(
        @JsonProperty("district")
        @NotBlank(message = DirectoryFederalDistrictMessage.DISTRICT_NOT_BLANK)
        @Length(max = 50, message = DirectoryFederalDistrictMessage.DISTRICT_LENGTH)
        String district
) {
}
