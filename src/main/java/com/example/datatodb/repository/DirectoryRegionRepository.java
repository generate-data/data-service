package com.example.datatodb.repository;

import com.example.datatodb.model.DirectoryRegion;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DirectoryRegionRepository extends JpaRepository<DirectoryRegion, Integer> {
    Optional<DirectoryRegion> findById(Integer id);

    Optional<DirectoryRegion> findByRegion(String region);
}
