package com.example.datatodb.repository;

import com.example.datatodb.model.Partner;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Repository
public interface PartnersRepository extends JpaRepository<Partner, UUID> {
    Optional<Partner> findByKey(String key);

    @Query("select p from Partner p "
            + "where (:key is null or p.key =:key)"
            + "and (:partnerName is null or lower(p.partnerName) =lower(:partnerName))"
            + "and (:partnerInn is null or p.partnerInn =:partnerInn)"
            + "and (:active is null or p.active =:active)")
    List<Partner> advancedSearch(
            String key,
            String partnerName,
            String partnerInn,
            Integer active);

}
