package com.example.datatodb.repository;

import com.example.datatodb.model.DirectoryCity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;
import java.util.UUID;

@Repository
public interface DirectoryCityRepository extends JpaRepository<DirectoryCity, UUID> {
    Optional<DirectoryCity> findById(Integer id);

    Optional<DirectoryCity> findByCity(String city);

    boolean deleteById(int id);
}
