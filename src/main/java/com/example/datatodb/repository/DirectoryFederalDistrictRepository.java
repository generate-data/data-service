package com.example.datatodb.repository;

import com.example.datatodb.model.DirectoryFederalDistrict;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface DirectoryFederalDistrictRepository extends JpaRepository<DirectoryFederalDistrict, Integer> {
    Optional<DirectoryFederalDistrict> findById(Integer id);

    Optional<DirectoryFederalDistrict> findByDistrict(String district);
}
