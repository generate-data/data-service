package com.example.datatodb.controller;

import com.example.datatodb.constants.url.ApiUrl;
import com.example.datatodb.constants.url.PartnerUrl;
import com.example.datatodb.dto.Partner.AddPartnerRequestDTO;
import com.example.datatodb.dto.Partner.GetPartnerAdvancedSearchRequestDTO;
import com.example.datatodb.dto.Partner.UpdatePartnerRequestDTO;
import com.example.datatodb.service.PartnerService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(ApiUrl.GET_API + ApiUrl.GET_VERSION)
@RequiredArgsConstructor
@Tag(name = "Таблица \"Контрагенты\"")
public class PartnerController {

    private final PartnerService partnerService;

    @GetMapping(PartnerUrl.GET_ALL)
    @Operation(summary = "Получить все записи")
    public ResponseEntity<?> findAllPartners() {
        log.debug("READ ALL PARTNERS");
        var response = partnerService.findAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping(PartnerUrl.GET_BY_KEY)
    @Operation(summary = "Получить запись по key")
    public ResponseEntity<?> findPartnerByKey(@RequestParam(name = "key") String key) {
        log.debug("READ PARTNER BY KEY");
        var response = partnerService.findByKey(key);
        return ResponseEntity.ok(response);
    }

    @PostMapping(PartnerUrl.GET_ADVANCED_SEARCH)
    @Operation(summary = "Получить запись по расширенному поиску")
    public ResponseEntity<?> findPartnerByAdvancedSearch(@RequestBody GetPartnerAdvancedSearchRequestDTO request) {
        log.debug("READ PARTNER BY ADVANCED SEARCH");
        var response = partnerService.findByAdvancedSearch(request);
        return ResponseEntity.ok(response);
    }

    @PostMapping(PartnerUrl.GET_ALL)
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<?> addNewPartner(@RequestBody @Valid AddPartnerRequestDTO request) {
        log.debug("ADD NEW PARTNER");
        var response = partnerService.addNew(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(PartnerUrl.GET_ALL)
    @Operation(summary = "Изменить запись по key")
    public ResponseEntity<?> updatePartner(@RequestBody UpdatePartnerRequestDTO request) {
        log.debug("UPDATE PARTNER");
        var response = partnerService.updatePartner(request);
        return ResponseEntity.ok(response);
    }

}
