package com.example.datatodb.controller;

import com.example.datatodb.constants.url.ApiUrl;
import com.example.datatodb.constants.url.DirectoryCityUrl;
import com.example.datatodb.dto.DirectoryCity.AddDirectoryCityRequestDTO;
import com.example.datatodb.dto.DirectoryCity.UpdateDirectoryCityRequestDTO;
import com.example.datatodb.service.DirectoryCityService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(ApiUrl.GET_API + ApiUrl.GET_VERSION)
@RequiredArgsConstructor
@Tag(name = "Справочник \"Города РФ\"")
public class DirectoryCityController {

    private final DirectoryCityService directoryCityService;

    @GetMapping(DirectoryCityUrl.GET_ALL)
    @Operation(summary = "Получить все записи")
    public ResponseEntity<?> findAllCities() {
        log.debug("READ ALL DIRECTORY CITIES");
        var response = directoryCityService.findAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping(DirectoryCityUrl.GET_BY_CITY)
    @Operation(summary = "Получить запись по названию города")
    public ResponseEntity<?> findByCityName(@RequestParam(name = "city") String city) {
        log.debug("READ DIRECTORY CITY BY NAME");
        var response = directoryCityService.findByCityName(city);
        return ResponseEntity.ok(response);
    }

    @PostMapping(DirectoryCityUrl.GET_ALL)
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<?> addNewDirectoryCity(@RequestBody @Valid AddDirectoryCityRequestDTO request) {
        log.debug("ADD NEW DIRECTORY CITY");
        var response = directoryCityService.addNew(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(DirectoryCityUrl.GET_ALL)
    @Operation(summary = "Изменить запись по id")
    public ResponseEntity<?> updateDirectoryCity(@RequestBody @Valid UpdateDirectoryCityRequestDTO request) {
        log.debug("UPDATE DIRECTORY CITY");
        var response = directoryCityService.update(request);
        return ResponseEntity.ok(response);
    }

}
