package com.example.datatodb.controller;

import com.example.datatodb.constants.url.ApiUrl;
import com.example.datatodb.constants.url.DirectoryRegionUrl;
import com.example.datatodb.dto.DirectoryRegion.AddDirectoryRegionRequestDTO;
import com.example.datatodb.dto.DirectoryRegion.UpdateDirectoryRegionRequestDTO;
import com.example.datatodb.service.DirectoryRegionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(ApiUrl.GET_API + ApiUrl.GET_VERSION)
@RequiredArgsConstructor
@Tag(name = "Справочник \"Регионы РФ\"")
public class DirectoryRegionController {

    private final DirectoryRegionService directoryRegionService;

    @GetMapping(DirectoryRegionUrl.GET_ALL)
    @Operation(summary = "Получить все записи")
    public ResponseEntity<?> findAllRegions() {
        log.debug("READ ALL DIRECTORY REGIONS");
        var response = directoryRegionService.findAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping(DirectoryRegionUrl.GET_BY_REGION)
    @Operation(summary = "Получить запись по названию региона")
    public ResponseEntity<?> findByRegionName(@RequestParam(name = "region") String region) {
        log.debug("READ DIRECTORY REGION BY NAME");
        var response = directoryRegionService.findByRegionName(region);
        return ResponseEntity.ok(response);
    }

    @PostMapping(DirectoryRegionUrl.GET_ALL)
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<?> addNewDirectoryRegion(@RequestBody @Valid AddDirectoryRegionRequestDTO request) {
        log.debug("ADD NEW DIRECTORY REGION");
        var response = directoryRegionService.addNew(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(DirectoryRegionUrl.GET_ALL)
    @Operation(summary = "Обновить запись по id")
    public ResponseEntity<?> updateDirectoryRegion(@RequestBody @Valid UpdateDirectoryRegionRequestDTO request) {
        log.debug("UPDATE DIRECTORY REGION");
        var response = directoryRegionService.update(request);
        return ResponseEntity.ok(response);
    }


}
