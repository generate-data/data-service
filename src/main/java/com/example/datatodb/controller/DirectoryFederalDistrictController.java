package com.example.datatodb.controller;

import com.example.datatodb.constants.url.ApiUrl;
import com.example.datatodb.constants.url.DirectoryFederalDistrictUrl;
import com.example.datatodb.dto.DirectoryFederalDistrict.AddDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.UpdateDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.service.DirectoryFederalDistrictService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Slf4j
@RestController
@RequestMapping(ApiUrl.GET_API + ApiUrl.GET_VERSION)
@RequiredArgsConstructor
@Tag(name = "Справочник \"Федеральные округа РФ\"")
public class DirectoryFederalDistrictController {

    private final DirectoryFederalDistrictService directoryFederalDistrictService;

    @GetMapping(DirectoryFederalDistrictUrl.GET_ALL)
    @Operation(summary = "Получить все записи")
    public ResponseEntity<?> findAllDistricts() {
        log.debug("READ ALL DIRECTORY FEDERAL DISTRICTS");
        var response = directoryFederalDistrictService.findAll();
        return ResponseEntity.ok(response);
    }

    @GetMapping(DirectoryFederalDistrictUrl.GET_BY_DISTRICT)
    @Operation(summary = "Получить запись по названию федерального округа")
    public ResponseEntity<?> findByCityName(@RequestParam(name = "district") String district) {
        log.debug("READ DIRECTORY FEDERAL DISTRICT BY NAME");
        var response = directoryFederalDistrictService.findByDistrictName(district);
        return ResponseEntity.ok(response);
    }

    @PostMapping(DirectoryFederalDistrictUrl.GET_ALL)
    @Operation(summary = "Добавить новую запись")
    public ResponseEntity<?> addNewDirectoryFederalDistrict(@RequestBody @Valid AddDirectoryFederalDistrictRequestDTO request) {
        log.debug("ADD NEW DIRECTORY FEDERAL DISTRICT");
        var response = directoryFederalDistrictService.addNew(request);
        return ResponseEntity.status(HttpStatus.CREATED).body(response);
    }

    @PatchMapping(DirectoryFederalDistrictUrl.GET_ALL)
    @Operation(summary = "Изменить запись по id")
    public ResponseEntity<?> updateDirectoryFederalDistrict(@RequestBody @Valid UpdateDirectoryFederalDistrictRequestDTO request) {
        log.debug("UPDATE DIRECTORY FEDERAL DISTRICT");
        var response = directoryFederalDistrictService.update(request);
        return ResponseEntity.ok(response);
    }

}
