package com.example.datatodb.model;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "generate_directory_cities")
public class DirectoryCity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Column(name = "city")
    private String city;
}
