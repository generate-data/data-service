package com.example.datatodb.model;

import lombok.*;

import javax.persistence.*;
import java.time.ZonedDateTime;

@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "generate_directory_regions")
public class DirectoryRegion {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Column(name = "region")
    private String region;
}
