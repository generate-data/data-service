package com.example.datatodb.model;

import lombok.*;
import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

import javax.persistence.*;
import java.time.ZonedDateTime;
import java.util.UUID;

@Builder
@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "GENERATE_PARTNERS")
public class Partner {
    @Id
    @Column(name = "id")
    @GenericGenerator(name = "uuid2", strategy = "uuid2")
    @GeneratedValue(strategy = GenerationType.IDENTITY, generator = "uuid2")
    @Type(type = "org.hibernate.type.PostgresUUIDType")
    private UUID id;

    @Column(name = "partner_key")
    private String key;

    @Column(name = "creation_date")
    private ZonedDateTime creationDate;

    @Column(name = "partner_name")
    private String partnerName;

    @Column(name = "partner_inn")
    private String partnerInn;

    @Column(name = "active")
    private Integer active;

}
