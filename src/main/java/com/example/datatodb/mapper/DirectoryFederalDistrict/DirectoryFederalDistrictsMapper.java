package com.example.datatodb.mapper.DirectoryFederalDistrict;

import com.example.datatodb.dto.DirectoryFederalDistrict.GetDirectoryFederalDistrictResponseDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.GetDirectoryFederalDistrictsResponseDTO;
import com.example.datatodb.model.DirectoryFederalDistrict;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DirectoryFederalDistrictsMapper {
    public GetDirectoryFederalDistrictsResponseDTO mapToDTO(List<DirectoryFederalDistrict> all) {
        List<GetDirectoryFederalDistrictResponseDTO> list = new ArrayList<>();

        all.forEach(district -> list.add(new GetDirectoryFederalDistrictResponseDTO(district.getId(), district.getDistrict())));
        return new GetDirectoryFederalDistrictsResponseDTO(list);
    }
}
