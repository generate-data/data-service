package com.example.datatodb.mapper.DirectoryFederalDistrict;

import com.example.datatodb.dto.DirectoryFederalDistrict.UpdateDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.model.DirectoryFederalDistrict;
import org.springframework.stereotype.Component;

@Component
public class UpdateDirectoryFederalDistrictMapper {
    public DirectoryFederalDistrict copyToEntity(UpdateDirectoryFederalDistrictRequestDTO dto, DirectoryFederalDistrict entity) {
        entity.setDistrict(dto.district());
        return entity;
    }
}
