package com.example.datatodb.mapper.DirectoryFederalDistrict;

import com.example.datatodb.dto.DirectoryFederalDistrict.AddDirectoryFederalDistrictRequestDTO;
import com.example.datatodb.dto.DirectoryFederalDistrict.GetDirectoryFederalDistrictResponseDTO;
import com.example.datatodb.model.DirectoryFederalDistrict;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DirectoryFederalDistrictMapper {
    GetDirectoryFederalDistrictResponseDTO toDTO(DirectoryFederalDistrict entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    DirectoryFederalDistrict toEntity(AddDirectoryFederalDistrictRequestDTO entity);
}
