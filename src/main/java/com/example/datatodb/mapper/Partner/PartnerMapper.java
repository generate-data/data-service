package com.example.datatodb.mapper.Partner;

import com.example.datatodb.dto.Partner.AddPartnerRequestDTO;
import com.example.datatodb.dto.Partner.GetPartnerResponseDTO;
import com.example.datatodb.dto.Partner.UpdatePartnerRequestDTO;
import com.example.datatodb.model.Partner;
import org.springframework.stereotype.Component;

@Component
public class PartnerMapper {

    public GetPartnerResponseDTO ToDTO(Partner partner) {

        return GetPartnerResponseDTO.builder()
                .key(partner.getKey())
                .creationDate(partner.getCreationDate().toString())
                .partnerName(partner.getPartnerName())
                .partnerInn(partner.getPartnerInn())
                .active(partner.getActive())
                .build();

    }

    public Partner ToEntity(AddPartnerRequestDTO request) {

        return Partner.builder()
                .partnerName(request.partnerName())
                .partnerInn(request.partnerInn())
                .build();
    }

    public Partner copyToEntity(UpdatePartnerRequestDTO objectDTO, Partner entity) {
        entity.setPartnerName(objectDTO.partnerName());
        entity.setPartnerInn(objectDTO.partnerInn());
        entity.setActive(objectDTO.active());
        return entity;
    }
}
