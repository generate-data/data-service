package com.example.datatodb.mapper.Partner;

import com.example.datatodb.dto.Partner.GetPartnerResponseDTO;
import com.example.datatodb.dto.Partner.GetPartnersResponseDTO;
import com.example.datatodb.model.Partner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class PartnersMapper {

    public GetPartnersResponseDTO mapToDTO(List<Partner> all) {
        GetPartnersResponseDTO response;
        List<GetPartnerResponseDTO> list = new ArrayList<>();

        all.forEach(partner -> list.add(
                GetPartnerResponseDTO.builder()
                        .key(partner.getKey())
                        .creationDate(partner.getCreationDate().toString())
                        .partnerName(partner.getPartnerName())
                        .partnerInn(partner.getPartnerInn())
                        .active(partner.getActive())
                        .build()
        ));
        response = new GetPartnersResponseDTO(list);
        return response;

    }
}
