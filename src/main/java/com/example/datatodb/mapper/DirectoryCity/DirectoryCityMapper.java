package com.example.datatodb.mapper.DirectoryCity;

import com.example.datatodb.dto.DirectoryCity.AddDirectoryCityRequestDTO;
import com.example.datatodb.dto.DirectoryCity.GetDirectoryCityResponseDTO;
import com.example.datatodb.model.DirectoryCity;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface DirectoryCityMapper {

    GetDirectoryCityResponseDTO toDTO(DirectoryCity entity);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "creationDate", ignore = true)
    DirectoryCity toEntity(AddDirectoryCityRequestDTO dto);
}
