package com.example.datatodb.mapper.DirectoryCity;

import com.example.datatodb.dto.DirectoryCity.UpdateDirectoryCityRequestDTO;
import com.example.datatodb.model.DirectoryCity;
import org.springframework.stereotype.Component;

@Component
public class UpdateDirectoryCityMapper {
    public DirectoryCity copyToEntity(UpdateDirectoryCityRequestDTO dto, DirectoryCity entity) {
        entity.setCity(dto.city());
        return entity;
    }
}
