package com.example.datatodb.mapper.DirectoryCity;

import com.example.datatodb.dto.DirectoryCity.GetDirectoryCitiesResponseDTO;
import com.example.datatodb.dto.DirectoryCity.GetDirectoryCityResponseDTO;
import com.example.datatodb.model.DirectoryCity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DirectoryCitiesMapper {
    public GetDirectoryCitiesResponseDTO mapToDTO(List<DirectoryCity> all) {
        List<GetDirectoryCityResponseDTO> list = new ArrayList<>();

        all.forEach(city -> list.add(new GetDirectoryCityResponseDTO(city.getId(),city.getCity())));
        return new GetDirectoryCitiesResponseDTO(list);
    }
}
