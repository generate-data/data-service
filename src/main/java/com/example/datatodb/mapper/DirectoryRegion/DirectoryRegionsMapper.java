package com.example.datatodb.mapper.DirectoryRegion;

import com.example.datatodb.dto.DirectoryRegion.GetDirectoryRegionResponseDTO;
import com.example.datatodb.dto.DirectoryRegion.GetDirectoryRegionsResponseDTO;
import com.example.datatodb.model.DirectoryRegion;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class DirectoryRegionsMapper {
    public GetDirectoryRegionsResponseDTO mapToDTO(List<DirectoryRegion> all) {
        List<GetDirectoryRegionResponseDTO> list = new ArrayList<>();
        all.forEach(region -> list.add(new GetDirectoryRegionResponseDTO(region.getId(), region.getRegion())));
        return new GetDirectoryRegionsResponseDTO(list);
    }
}
