package com.example.datatodb.mapper.DirectoryRegion;

import com.example.datatodb.dto.DirectoryRegion.AddDirectoryRegionRequestDTO;
import com.example.datatodb.dto.DirectoryRegion.GetDirectoryRegionResponseDTO;
import com.example.datatodb.dto.DirectoryRegion.UpdateDirectoryRegionRequestDTO;
import com.example.datatodb.model.DirectoryRegion;
import org.springframework.stereotype.Component;

@Component
public class DirectoryRegionMapper {
    public GetDirectoryRegionResponseDTO mapToDTO(DirectoryRegion directoryRegion) {
        return new GetDirectoryRegionResponseDTO(directoryRegion.getId(), directoryRegion.getRegion());
    }

    public DirectoryRegion ToEntity(AddDirectoryRegionRequestDTO request) {
        return DirectoryRegion.builder()
                .region(request.region().trim())
                .build();
    }

    public DirectoryRegion copyToEntity(UpdateDirectoryRegionRequestDTO objectDTO, DirectoryRegion entity) {
        entity.setRegion(objectDTO.region());
        return entity;
    }
}
