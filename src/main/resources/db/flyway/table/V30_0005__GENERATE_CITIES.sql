----------------------------------------------------------
-- Таблица "Города РФ"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_CITIES
(
    ID                  UUID PRIMARY KEY NOT NULL default gen_random_uuid(), -- Ключ записи
    CREATION_DATE       TIMESTAMP(6)     NOT NULL DEFAULT NOW(),             -- Дата создания записи
    CITY_ID             INT              NOT NULL,                           -- Внешний ключ на справочник "Города РФ"
    REGION_ID           INT              NOT NULL,                           -- Внешний ключ на справочник "Регионы РФ"
    FEDERAL_DISTRICT_ID INT              NOT NULL,                           -- Внешний ключ на справочник "Федеральные округа РФ"

    CONSTRAINT FK_CITY_ID
        FOREIGN KEY (CITY_ID)
            REFERENCES GENERATE_DIRECTORY_CITIES (ID)
            ON DELETE CASCADE,
    CONSTRAINT FK_REGION_ID
        FOREIGN KEY (REGION_ID)
            REFERENCES GENERATE_DIRECTORY_REGIONS (ID)
            ON DELETE CASCADE,
    CONSTRAINT FK_FEDERAL_DISTRICT_ID
        FOREIGN KEY (FEDERAL_DISTRICT_ID)
            REFERENCES GENERATE_DIRECTORY_FEDERAL_DISTRICTS (ID)
            ON DELETE CASCADE
);

COMMENT
    ON TABLE GENERATE_CITIES IS 'Таблица "Города РФ"';
COMMENT
    ON COLUMN GENERATE_CITIES.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_CITIES.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_CITIES.CITY_ID IS 'Внешний ключ на справочник "Города РФ"';
COMMENT
    ON COLUMN GENERATE_CITIES.REGION_ID IS 'Внешний ключ на справочник "Регионы РФ"';
COMMENT
    ON COLUMN GENERATE_CITIES.FEDERAL_DISTRICT_ID IS 'Внешний ключ на справочник "Федеральные округа РФ"';
