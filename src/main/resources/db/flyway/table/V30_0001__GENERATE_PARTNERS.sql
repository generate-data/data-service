----------------------------------------------------------
-- Таблица "Контрагенты"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_PARTNERS
(
    ID            UUID PRIMARY KEY NOT NULL default gen_random_uuid(), -- Ключ записи
    PARTNER_KEY   VARCHAR          NOT NULL UNIQUE,                    -- Ключ партнера
    CREATION_DATE TIMESTAMP(6)     NOT NULL DEFAULT NOW(),             -- Дата создания записи
    PARTNER_NAME  VARCHAR          NOT NULL,                           -- Наименование контрагента
    PARTNER_INN   VARCHAR          NOT NULL,                           -- ИНН контрагента
    ACTIVE        INTEGER          NOT NULL DEFAULT 1                  -- Признак активности контрагента: 0 - не активен, 1 - активен
);

COMMENT
    ON TABLE GENERATE_PARTNERS IS 'Контрагенты';
COMMENT
    ON COLUMN GENERATE_PARTNERS.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_PARTNERS.PARTNER_KEY IS 'Ключ партнера';
COMMENT
    ON COLUMN GENERATE_PARTNERS.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_PARTNERS.PARTNER_NAME IS 'Наименование контрагента';
COMMENT
    ON COLUMN GENERATE_PARTNERS.PARTNER_INN IS 'ИНН контрагента';
COMMENT
    ON COLUMN GENERATE_PARTNERS.ACTIVE IS 'Признак активности контрагента: 0 - не активен, 1 - активен';