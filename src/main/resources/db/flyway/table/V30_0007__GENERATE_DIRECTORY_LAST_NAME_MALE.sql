----------------------------------------------------------
-- Справочник "Мужские фамилии"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_DIRECTORY_LAST_NAME_MALE
(
    ID            SERIAL PRIMARY KEY NOT NULL,               -- Ключ записи
    CREATION_DATE TIMESTAMP(6)       NOT NULL DEFAULT NOW(), -- Дата создания записи
    LAST_NAME     VARCHAR            NOT NULL UNIQUE         -- Фамилия
);

COMMENT
    ON TABLE GENERATE_DIRECTORY_FEDERAL_DISTRICTS IS 'Справочник "Мужские фамилии"';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.DISTRICT IS 'Фамилия';
