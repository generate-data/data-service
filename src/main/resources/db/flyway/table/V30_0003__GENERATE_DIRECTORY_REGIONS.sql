----------------------------------------------------------
-- Справочник "Регионы РФ"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_DIRECTORY_REGIONS
(
    ID            SERIAL PRIMARY KEY NOT NULL,               -- Ключ записи
    CREATION_DATE TIMESTAMP(6)       NOT NULL DEFAULT NOW(), -- Дата создания записи
    REGION        VARCHAR            NOT NULL UNIQUE         -- Название региона РФ
);

COMMENT
    ON TABLE GENERATE_DIRECTORY_REGIONS IS 'Справочник "Регионы РФ"';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_REGIONS.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_REGIONS.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_REGIONS.REGION IS 'Наименование региона РФ';
