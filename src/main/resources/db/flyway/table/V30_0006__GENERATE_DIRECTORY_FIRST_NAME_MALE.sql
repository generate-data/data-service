----------------------------------------------------------
-- Справочник "Мужские имена"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_DIRECTORY_FIRST_NAME_MALE
(
    ID            SERIAL PRIMARY KEY NOT NULL,               -- Ключ записи
    CREATION_DATE TIMESTAMP(6)       NOT NULL DEFAULT NOW(), -- Дата создания записи
    FIRST_NAME    VARCHAR            NOT NULL UNIQUE         -- Имя
);

COMMENT
    ON TABLE GENERATE_DIRECTORY_FEDERAL_DISTRICTS IS 'Справочник "Мужские имена"';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.DISTRICT IS 'Имя';
