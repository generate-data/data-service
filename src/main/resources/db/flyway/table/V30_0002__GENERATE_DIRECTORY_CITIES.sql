----------------------------------------------------------
-- Справочник "Города РФ"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_DIRECTORY_CITIES
(
    ID            SERIAL PRIMARY KEY NOT NULL,               -- Ключ записи
    CREATION_DATE TIMESTAMP(6)       NOT NULL DEFAULT NOW(), -- Дата создания записи
    CITY          VARCHAR UNIQUE     NOT NULL                -- Название города РФ
);

COMMENT
    ON TABLE GENERATE_DIRECTORY_CITIES IS 'Справочник "Города РФ"';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_CITIES.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_CITIES.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_CITIES.CITY IS 'Наименование города';
