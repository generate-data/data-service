----------------------------------------------------------
-- Справочник "Федеральные округа РФ"
----------------------------------------------------------

CREATE TABLE IF NOT EXISTS GENERATE_DIRECTORY_FEDERAL_DISTRICTS
(
    ID            SERIAL PRIMARY KEY NOT NULL,               -- Ключ записи
    CREATION_DATE TIMESTAMP(6)       NOT NULL DEFAULT NOW(), -- Дата создания записи
    DISTRICT      VARCHAR            NOT NULL UNIQUE         -- Название федерального округа
);

COMMENT
    ON TABLE GENERATE_DIRECTORY_FEDERAL_DISTRICTS IS 'Справочник "Федеральные округа РФ"';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.ID IS 'Ключ записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.CREATION_DATE IS 'Дата создания записи';
COMMENT
    ON COLUMN GENERATE_DIRECTORY_FEDERAL_DISTRICTS.DISTRICT IS 'Название федерального округа';
