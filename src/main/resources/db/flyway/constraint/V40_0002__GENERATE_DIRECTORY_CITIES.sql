----------------------------------------------------------
-- Справочник "Города РФ"
----------------------------------------------------------
ALTER TABLE GENERATE_DIRECTORY_CITIES
    ADD CONSTRAINT VALID_CITY
        CHECK (length(city) <= 50);

COMMENT ON CONSTRAINT VALID_ACTIVE ON GENERATE_PARTNERS IS 'Поле "ACTIVE" Допустимое значение 0 или 1';
COMMENT ON CONSTRAINT VALID_INN ON GENERATE_PARTNERS IS 'Поле "PARTNER_INN" Должно быть меньше 11 символов';