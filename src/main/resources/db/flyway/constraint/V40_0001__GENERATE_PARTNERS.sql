----------------------------------------------------------
-- Таблица "Контрагенты"
----------------------------------------------------------
ALTER TABLE GENERATE_PARTNERS
    ADD CONSTRAINT VALID_ACTIVE
        CHECK (ACTIVE IN (0, 1)),
    ADD CONSTRAINT VALID_INN
        CHECK (length(PARTNER_INN) < 11 );

COMMENT ON CONSTRAINT VALID_ACTIVE ON GENERATE_PARTNERS IS 'Поле "ACTIVE" Допустимое значение 0 или 1';
COMMENT ON CONSTRAINT VALID_INN ON GENERATE_PARTNERS IS 'Поле "PARTNER_INN" Должно быть меньше 11 символов';