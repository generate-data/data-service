package com.example.datatodb;

import com.example.datatodb.model.DirectoryCity;
import com.example.datatodb.repository.DirectoryCityRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.transaction.annotation.Transactional;

import java.time.ZonedDateTime;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class DataToDbApplicationTests {

    @Autowired
    private DirectoryCityRepository repository;


    @Order(1)
    @Test
    @DisplayName("Should add new City")
    @Transactional
    void addNewCity() {
        repository.save(DirectoryCity.builder()
                .city("Горгород")
                .creationDate(ZonedDateTime.now())
                .build());
    }

    @Order(2)
    @Test
    @DisplayName("Should find city by name")
    void findCity() {
        Optional<DirectoryCity> optional = repository.findByCity("Горгород");
        optional.ifPresent(directoryCity -> assertEquals("Горгород", directoryCity.getCity()));
    }

    @Order(3)
    @Test
    @DisplayName("Should delete City")
    @Transactional
    void deleteCity() {
        Optional<DirectoryCity> optional = repository.findByCity("Горгород");
        optional.ifPresent(directoryCity -> assertTrue(repository.deleteById(directoryCity.getId())));
    }

    @Order(4)
    @Test
    @DisplayName("Should not find city by name")
    void findDeletedCity() {
        Optional<DirectoryCity> optional = repository.findByCity("Горгород");
        assertFalse(optional.isPresent());
    }
}
